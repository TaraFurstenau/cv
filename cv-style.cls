% -- Encoding UTF-8 without BOM

\ProvidesClass{cv-style}[2015/02/27 CV class]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption{espanol}{\def\@cv@espanol{}}
\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}
\ProcessOptions\relax
\LoadClass{article}


%----------------------------------------------------------------------------------------
% Colors %
%----------------------------------------------------------------------------------------

\RequirePackage{xcolor}

\definecolor{white}{RGB}{255,255,255}

\definecolor{darkgray}{HTML}{333333}
\definecolor{gray}{HTML}{5A5A5A}
\definecolor{black}{HTML}{000000}
\definecolor{lightgray}{HTML}{818181}
\definecolor{midgray}{HTML}{787878}
\definecolor{darkteal}{HTML}{03434B}
\definecolor{teal}{HTML}{056571}
\definecolor{lightteal}{HTML}{50939B}
\definecolor{dark}{HTML}{565656}
\definecolor{light}{HTML}{f7f7f7}
\definecolor{maroon}{HTML}{711105}
\definecolor{seafoam}{HTML}{5AA2AA}
\definecolor{date}{HTML}{A4A4A4}


%%% Grayscale
% \colorlet{secondary}{black}
% \colorlet{tertiary}{black}
% \colorlet{linc}{black}
% \colorlet{header}{black}
% \colorlet{fillheader}{white}
% \colorlet{textcolor}{black}
% \colorlet{headercolor}{black}

%%% Teal palette
\colorlet{secondary}{darkteal}
\colorlet{tertiary}{teal}
\colorlet{linc}{lightteal}
\colorlet{header}{white}
\colorlet{fillheader}{teal}
\colorlet{textcolor}{darkgray}
\colorlet{headercolor}{darkteal}
%----------------------------------------------------------------------------------------
% Fonts %
%----------------------------------------------------------------------------------------

\RequirePackage[quiet]{fontspec}
\RequirePackage[math-style=TeX,vargreek-shape=unicode]{unicode-math}

\newfontfamily\bodyfont{Roboto-Regular}[Path=fonts/]
\newfontfamily\bodyfontit{Roboto-LightItalic}[Path=fonts/]
\newfontfamily\thinfont{Roboto-Thin}[Path=fonts/]
\newfontfamily\headingfont{RobotoCondensed-Bold}[Path=fonts/]

\defaultfontfeatures{Mapping=tex-text}
\setmainfont[Mapping=tex-text, Color=textcolor, Path = fonts/]{Roboto-Light}

\newcommand{\italicasecond}[1]{%
    {\color{secondary}\bodyfontit #1}%
}
\newcommand{\italica}[1]{%
    {\color{gray}\bodyfontit #1}%
}
%----------------------------------------------------------------------------------------
% Header %
%----------------------------------------------------------------------------------------

\RequirePackage{tikz}

\newcommand{\header}[2]{%
  \begin{tikzpicture}[remember picture,overlay]
    \node [rectangle, fill=fillheader, anchor=north, minimum width=\paperwidth, minimum height=2cm] (box) at (current page.north){};
    \node [anchor=center] (name) at (box) {%
      \fontsize{40pt}{65pt}\color{header}%
      {\thinfont #1}{\bodyfont  #2}
    };
  \end{tikzpicture}
  \vspace{1cm}
  \vspace{-2\parskip}
}

%----------------------------------------------------------------------------------------
%	Last updated command %
%----------------------------------------------------------------------------------------

\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{polyglossia}

\newcommand{\sethyphenation}[3][]{%
  \sbox0{\begin{otherlanguage}[#1]{#2}
    \hyphenation{#3}\end{otherlanguage}}}
%\sethyphenation[<options>]{<language>}{<list of words separated by spaces>}

\ifdefined\@cv@espanol
  \setdefaultlanguage{spanish}
  \def\lastupdatedtext{Última Actualización el}
\else
  \setdefaultlanguage[variant=british]{english}
  \def\lastupdatedtext{Last Updated on}
\fi

\setlength{\TPHorizModule}{0.01\paperwidth}
\setlength{\TPVertModule}{0.01\paperwidth}

\newcommand{\lastupdated}{ 
  \begin{textblock}{10}(9, 27)
    \raggedleft
    \fontsize{8pt}{10pt}\color{date}\thinfont 
    \lastupdatedtext{} \today
  \end{textblock}}

%----------------------------------------------------------------------------------------
% Structure %
%----------------------------------------------------------------------------------------
\RequirePackage{parskip}
\RequirePackage{needspace}

% \newcounter{colorCounter}
\def\@sectioncolor{%
    \needspace{1\baselineskip}\titlerule\vspace{.1ex}\color{headercolor}%
%   \stepcounter{colorCounter}%
}

\def\@subsectioncolor{%
    \needspace{1\baselineskip}\titlerule\vspace{.1ex}\color{tertiary}%
%   \stepcounter{colorCounter}%
}

\renewcommand{\section}[1]{
  {\par\vspace{\parskip}
  {%
    \LARGE\headingfont\color{headercolor}%
    \@sectioncolor #1%
  }
  \par\vspace{\parskip}}
}



\renewcommand{\subsection}[1]{
  {\par\vspace{.1\parskip}
  {%
    \Large\headingfont\color{tertiary}%
    \@subsectioncolor #1%
  }
  \par\vspace{.25\parskip}}
}


\newcommand{\jobtitle}[1]{%
    {\color{gray}\bodyfontit #1}%
}

\pagestyle{empty}

%----------------------------------------------------------------------------------------
% List environment %
%----------------------------------------------------------------------------------------

\setlength{\tabcolsep}{3pt}
\newenvironment{entrylist}{%
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
}{%
  \end{tabular*}
}
\renewcommand{\bfseries}{\headingfont\color{secondary}}
\newcommand{\entry}[4]{%
  #1&\parbox[t]{12.3cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=secondary} #3}\\%
    #4\vspace{\parsep}%
  }\\}

%----------------------------------------------------------------------------------------
% Side block %
%----------------------------------------------------------------------------------------
\TPMargin{.1cm}
\setlength{\TPHorizModule}{1.01cm}
\setlength{\TPVertModule}{1.3cm}
\newenvironment{aside}{%
  \let\oldsection\section
  \renewcommand{\section}[1]{
    \par\vspace{\baselineskip}{\Large\headingfont\color{secondary} ##1}
  }
    \renewcommand{\subsection}[1]{
    \par\vspace{\baselineskip}{\large\headingfont\color{tertiary} ##1}
  }
  \begin{textblock}{5.1}(.5, 2.1)
 
  \textblockcolour{light}
  \begin{flushleft}
  \obeycr
}{%
  \restorecr
  \end{flushleft}
  \end{textblock}
  \let\section\oldsection
}

%----------------------------------------------------------------------------------------
% Other tweaks %
%----------------------------------------------------------------------------------------

\RequirePackage[left=6cm,top=1cm,right=1cm,bottom=1cm,nohead,nofoot]{geometry}
\RequirePackage{hyperref}
\hypersetup{
    pdftitle={CV/Resume \textbar{} Tara Furstenau},
    pdfauthor={Tara Furstenau},
    pdfsubject={CV/Resume},
    colorlinks=true,
    urlcolor=linc
    }